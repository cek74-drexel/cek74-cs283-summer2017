#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

void selectn(char *ln)//SELECT * FROM TableName WHERE Field1=value
{
    //finds all words and values
    char* sel = strtok(ln, " ");
    char* star = strtok(NULL, " ");
    char* frm = strtok(NULL, " ");
    char* tbname = strtok(NULL, " ");
    char* whr = strtok(NULL, " ");
    char* fld = strtok(NULL, "\n");
    
    char* fld1 = strtok(fld, "\"=");
    char* val = strtok(NULL, "\n");
    
    //opens file making sure the file exists
    FILE *new;
    new = fopen(tbname, "r");
    if(new == NULL)
    {
        perror("Error opening file");
    }
    else
    {
        //declared variables
        int print;
        char str[600];
        int size = 1000;
        char** fds = (char**) malloc(size*sizeof(char*));
        char* fld;
        char info[1000];
        char** comp = (char**) malloc(size*sizeof(char*));
        char* place;
        //finding the fields and which one needs to be printed
        for(int i = 0; i < 1; i++)
        {
            if(fgets(str, 600, new)!= NULL)
            {
                //goes through all the fields
                fld = strtok(str, " ");
                for(int c = 0; fld != NULL; c++)
                {
                    fds[c] = (char*) malloc(size*sizeof(char));
                    strcpy(fds[c], fld);
                    if(strcmp(fds[c], fld1) == 0)
                    {
                        print = c;//saves the value of the field that wants to be printed
                    }
                    fld = strtok(NULL, " ");
                }
                
                for(int count = 0; fgets(info, 600, new)!= NULL; count++)
                {
                    place = strtok(info, " ");
                    for(int c = 0; place != NULL; c++)
                    {
                        comp[c] = (char*) malloc(size*sizeof(char));
                        strcpy(comp[c], place);
                        place = strtok(NULL, " ");
                    }
                    if(strcmp(comp[print], val) == 0)//if the value of the field in this array of values equals the values that needs to be printed, it prints the array
                    {
                        for(int c = 0; comp[c] != NULL; c++)
                        {
                            printf("%s ", comp[c]);
                        }
                        printf("\n");
                    }
                    
                    //freed and closed all mallocs and files
                    for(int i = 0; fds[i] != NULL; i++)
                    {
                        free(comp[i]);
                    }
                }
                free(comp);
                for(int i = 0; fds[i] != NULL; i++)
                {
                    free(fds[i]);
                }
                free(fds);
            }
        }
        fclose(new);
    }
}

void deleten(char *ln)//DELETE FROM TableName WHERE Field1=value
{
    //finds all words and values
    char* del = strtok(ln, " ");
    char* frm = strtok(NULL, " ");
    char* tbname = strtok(NULL, " ");
    char* whr = strtok(NULL, " ");
    char* fld = strtok(NULL, "\n");
    
    char* fld1 = strtok(fld, "\"=");
    char* val = strtok(NULL, "\n");
    
    //opens file making sure the file exists
    FILE *new;
    new = fopen(tbname, "r");
    if(new == NULL)
    {
        perror("Error opening file");
    }
    else
    {
        //declared variables
        int change;
        char str[600];
        int size = 1000;
        char** fds = (char**) malloc(size*sizeof(char*));
        char* fld;
        char info[1000];
        char** comp = (char**) malloc(size*sizeof(char*));
        char* place;
        //finding the fields and which one needs to be deleted
        for(int i = 0; i < 1; i++)
        {
            if(fgets(str, 600, new)!= NULL)
            {
                fld = strtok(str, " ");
                for(int c = 0; fld != NULL; c++)
                {
                    fds[c] = (char*) malloc(size*sizeof(char));
                    strcpy(fds[c], fld);
                    if(strcmp(fds[c], fld1) == 0)
                    {
                        change = c;//saves the field that needs to be deleted
                    }
                    fld = strtok(NULL, " ");
                }
                //begins writing to a new file to save info of other file (wont overwrite everything)
                FILE *files = fopen("temp.txt", "w");
                for(int c = 0; fds[c] != NULL; c++)
                {
                    fputs(fds[c], files);
                    if(fds[c+1] != NULL)
                        fputs(" ", files);
                }
                //goes through all the values in one array multiple times comparing values
                for(int count = 0; fgets(info, 600, new)!= NULL; count++)
                {
                    place = strtok(info, " ");
                    for(int c = 0; place != NULL; c++)
                    {
                        comp[c] = (char*) malloc(size*sizeof(char));
                        strcpy(comp[c], place);
                        place = strtok(NULL, " ");
                    }
                    if(strcmp(comp[change], val) != 0)//if the value of that field equals the value that needs to be deleted it doesn't write it it to the file, but it still deltes the malloc
                    {
                        for(int c = 0; comp[c] != NULL; c++)
                        {
                            fputs(comp[c], files);
                            if(fds[c+1] != NULL)
                                fputs(" ", files);
                        }
                    }
                    for(int i = 0; fds[i] != NULL; i++)
                    {
                        free(comp[i]);
                    }
                }
                free(comp);
                for(int i = 0; fds[i] != NULL; i++)
                {
                    free(fds[i]);
                }
                free(fds);
                fclose(files);
                //freed and closed all mallocs and files
            }
        }
        int rem = remove(tbname);
        if(rem == 0)
            rename("temp.txt", tbname);//renames file to the original table name
        fclose(new);
    }
}

void updaten(char *ln)//UPDATE TableName SET Field1=new_value WHERE Field2=value
{
    //finds all words and values
    char* up = strtok(ln, " ");
    char* tbname = strtok(NULL, " ");
    char* set = strtok(NULL, " ");
    char* flds1 = strtok(NULL, " ");
    char* whr = strtok(NULL, " ");
    char* flds2 = strtok(NULL, "\n");
    
    char* fld1 = strtok(flds1, "=");
    char* val1 = strtok(NULL, "\n");
    char* fld2 = strtok(flds2, "=");
    char* val2 = strtok(NULL, "\n");
    
    //opens file making sure the file exists
    FILE *new;
    new = fopen(tbname, "r");
    if(new == NULL)
    {
        perror("Error opening file");
    }
    else
    {
        //declared variables
        int change;
        int find;
        char str[600];
        int size = 1000;
        char** fds = (char**) malloc(size*sizeof(char*));
        char* fld;
        char info[1000];
        char** comp = (char**) malloc(size*sizeof(char*));
        char* place;
        //finding all the fields and which one needs to be found and which one needs to be changed
        for(int i = 0; i < 1; i++)
        {
            if(fgets(str, 600, new)!= NULL)
            {
                fld = strtok(str, " ");
                for(int c = 0; fld != NULL; c++)
                {
                    fds[c] = (char*) malloc(size*sizeof(char));
                    strcpy(fds[c], fld);
                    if(strcmp(fds[c], fld1) == 0)
                    {
                        //saves value
                        change = c;
                    }
                    else if(strcmp(fds[c], fld2) == 0)
                    {
                        //saves value
                        find = c;
                    }
                    fld = strtok(NULL, " ");
                }
                //begins writing to a new file to save info of other file (wont overwrite everything)
                FILE *files = fopen("temp.txt", "w");
                for(int c = 0; fds[c] != NULL; c++)
                {
                    fputs(fds[c], files);
                    if(fds[c+1] != NULL)
                        fputs(" ", files);
                }
                //goes through all the values in one array multiple times comparing values
                for(int count = 0; fgets(info, 600, new)!= NULL; count++)
                {
                    place = strtok(info, " ");
                    for(int c = 0; place != NULL; c++)
                    {
                        comp[c] = (char*) malloc(size*sizeof(char));
                        strcpy(comp[c], place);
                        place = strtok(NULL, " ");
                    }
                    //if the find value is the same as the row to change
                    if(strcmp(comp[find], val2) == 0)
                    {
                        free(comp[change]);//frees it first then switches the vlaue
                        comp[change] = val1;
                    }
                    //write all the values to the new file, including the new value
                    for(int c = 0; comp[c] != NULL; c++)
                    {
                        fputs(comp[c], files);
                        if(fds[c+1] != NULL)
                            fputs(" ", files);
                    }
                    for(int i = 0; fds[i] != NULL; i++)
                    {
                        free(comp[i]);
                    }
                }
                free(comp);
                for(int i = 0; fds[i] != NULL; i++)
                {
                    free(fds[i]);
                }
                free(fds);
                fclose(files);
                //freed and closed all mallocs and files
            }
        }
        int rem = remove(tbname);
        if(rem == 0)
            rename("temp.txt", tbname);//renames file to the original table name
        fclose(new);     
    }
    
}

void insertn(char *ln)//INSERT INTO TableName (Field1="value", Field2="value", ...)
{
    //finds words form command
    char* inst = strtok(ln, " ");
    char* into = strtok(NULL, " ");
    char* tbname = strtok(NULL, " ");
    char* flds = strtok(NULL, "\n");
    //opens file making sure it's a fi;e that already exists
    FILE *new;
    FILE *news;
    news = fopen(tbname, "r");
    if(news == NULL)
    {
        perror("Error opening file");
		  exit(1);
    }
    else
    {
        fclose(news);
        //re-opens for appending to the file
        new = fopen(tbname, "a");
        int size;
        //finds the amount of fields
        if((sizeof(flds)+1)%2 != 0)
            size = ((sizeof(flds)+1)/2)-1;
        char** words = (char**) malloc(size*sizeof(char*));
        //finds the field and it's value
        char* fld = strtok(flds, "(=, )");
        char* val = strtok(NULL, "(=, )");
        char line[20];
        for(int i = 0; fld != NULL && val != NULL; i++)
        {
            words[i] = (char*) malloc(size*sizeof(char));
            strcpy(words[i], val);
            fld = strtok(NULL, "(=,) ");
            val = strtok(NULL, "(=,) ");
        }
        //places the values in the fields
        for(int i = 0; i < size; i++)
        {
            strcpy(line, " ");
            strcat(words[i], line);
            fputs(words[i], new);
            free(words[i]);
        }
        free(words);
        //freed all the mallocs
	 }
    fclose(new);
}

void createn(char *ln)//CREATE TABLE TableName FIELDS [Field1, Field2, ...]
{
    //finds each word
    char* crt = strtok(ln, " ");
    char* tb = strtok(NULL, " ");
    char* tbname = strtok(NULL, " ");
    char* fld = strtok(NULL, " ");
    char* flds = strtok(NULL, "\n");
    //creates new file with name
    FILE *new;
    new = fopen(tbname ,"a");
    //puts the fields in the file
    char* newfld = strtok(flds, " [],\n");
    while(newfld != NULL)
    {
        fputs(newfld, new);
        newfld = strtok(NULL, "[],");
    }
    fclose(new);
}

void dropn(char *ln)//DROP TABLE Tablename
{
    //finds each word
    char* del = strtok(ln, " ");
    char* tb = strtok(NULL, " ");
    char* tbname = strtok(NULL, "\n");
    //removes table
    int rem = remove(tbname);
    if(rem == 0)
        printf("File successfully removed!\n");
    else
        printf("File unnsuccessfully removed or is not a table\n");

	
}


int main(int argc, char *argv[])
{
    //checks if directory is there, if not it makes it in the folder that it is in
    struct stat st = {0};
    char src[50], dest[50];
    strcpy(src, argv[1]);
    strcpy(dest, "../");
    strcat(dest, src);
    if (stat(dest, &st) == -1) {
        mkdir(argv[1], 0700);
    }
    
    //opens file for reading
    char const* const fileName = argv[2];
    FILE* file = fopen(fileName, "r");
    char line[256];
    
    //finds the first letter(s) of the first word of the command
    while(fgets(line, sizeof(line), file))
    {
        if(line[0] == 'S')
        {
           selectn(line);
        }
        else if(line[0] == 'D' && line[1] == 'E')
        {
           deleten(line);
        }
        else if(line[0] == 'U')
        {
            updaten(line);
        }
        else if(line[0] == 'I')
        {
            insertn(line);
        }
        else if(line[0] == 'C')
        {
            createn(line);
        }
        else if(line[0] == 'D' && line[1] == 'R')
        {
            dropn(line);
        }
        else
        {
            printf("Check argument placements/spelling\n");
        }
    }
    
	fclose(file);    
    return 0;
}
