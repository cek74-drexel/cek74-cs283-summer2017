#include <stdlib.h>
#include <stdio.h>

void swapz(int *x, int *y)//to swap the pointers
{
    int temp = *x;
    *x = *y;
    *y = temp;
}

//Bubble sort using pointer arithmetic
void sort(int* a, int size)
{
    for(int i = 0; i < size; i++)
    {
        for(int b = 0; b < size-i-1; b++)
        {
            if(*(a+b) > *(a+b+1))
            {
                swapz((a+b), (a+b+1));//using function
            }
        }
        size --;
    }
}

int main()
{
    int size = 5;
    int* a = (int*) malloc(size*sizeof(int));
    
    //I assigned values
    *(a+0) = 3;
    *(a+1) = 9;
    *(a+2) = 4;
    *(a+3) = 1;
    *(a+4) = 6;
    
    printf("The array list is ");//print original list
    for(int i = 0; i < size; i++)
    {
        printf("%d ", *(a+i));
    }
    printf("\n");
    
    sort(a, size);//sort
    
    printf("The array has been sorted into ");//print new list
    for(int i = 0; i < size; i++) {
        printf("%d ", *(a+i));
    }
    printf("\n");
    
    free(a);//free pointers
    return 0;
}
