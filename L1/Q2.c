#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main()
{
	char** a = (char**) malloc(10*sizeof(char*));//char** pointer that contains 10 char*'s
    

    for(int i = 0; i < 10; i++)
	{
		a[i] = (char*) malloc(15*sizeof(char));//initialize each of the 10 char*'s in a loop to a char array of size 15
	}
    
    for(int i = 0; i < 10; i++)
    {
        strncpy(a[i], "CS283LabOne", strlen("CS283LabOne") + 1);//initialize each to a word
    }
    
    for(int c = 0; c < 10; c++)//prints
    {
        printf("%s\n", a[c]);
    }
    
    for(int b = 0; b < 10; b++)//freeing
    {
        free(a[b]);
    }
    free(a);

    return 0;
}
