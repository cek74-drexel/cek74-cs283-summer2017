#include <stdlib.h>
#include <stdio.h>

struct ListNode {//define listnode
    int data;
    struct ListNode* next;
};


void swapz(struct ListNode* x, struct ListNode* y, struct ListNode* f)//function to swap the nodes itself
{
    struct ListNode* id = f;
    while(id != NULL)
    {
        if (id->next == x)
        {
            id->next = y;
        }
        id = id->next;
    }
    
    x->next = y->next;
    y->next = x;
    
}

//Bubble sort
void sort(struct ListNode* a, int size)
{
    int s = size;
    for(int i = 0; i <= s+1; i++)
    {
        struct ListNode* c = a;
        for(int b = 0; b < size - i; b++)
        {
            if(c->next != NULL)
            {
                if(c->data > c->next->data)
                {
                    swapz(c, c->next, a);
                    c = c->next;
                }
                else
                {
                    c = c->next;
                }
            }
            else
                break;
            
        }
        s --;
    }
}



int main()
{
    //defined size and values of each
    int size = 5;
    struct ListNode* t1 = (struct ListNode*) malloc(sizeof(struct
ListNode));
    struct ListNode* t2 = (struct ListNode*) malloc(sizeof(struct
ListNode));
    struct ListNode* t3 = (struct ListNode*) malloc(sizeof(struct
ListNode));
    struct ListNode* t4 = (struct ListNode*) malloc(sizeof(struct
ListNode));
    struct ListNode* t5 = (struct ListNode*) malloc(sizeof(struct
ListNode));
    t1->data = 3;
    t1->next = t2;
    t2->data = 9;
    t2->next = t3;
    t3->data = 4;
    t3->next = t4;
    t4->data = 1;
    t4->next = t5;
    t5->data = 6;
    t5->next = NULL;
    
    //print pointers
    struct ListNode* idx = t1;
    printf("The array list is ");
    while(idx != NULL)
    {
        printf("%d ", idx->data);
        idx = idx->next;
    }
    printf("\n");
    
    sort(t1, size);//sorts arraylist
    
    //prints new array except the first pointer that is at the beginnig for some reason
    struct ListNode* idy = t1;
    printf("The array list is sorted into ");
    while(idy != NULL)
    {
        printf("%d ", idy->data);
        idy = idy->next;
    }
    printf("\n");
    
    //frees everything
    free(idx);
    free(idy);
    free(t1);
    free(t2);
    free(t3);
    free(t4);
    free(t5);
    
    
    return 0;
}
