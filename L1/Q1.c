#include <stdlib.h>
#include <stdio.h>

int main ()
{
	int* b = (int*) malloc(10*sizeof(int));// def of int & creates array of 10 integers using malloc()
	int* addr;

	for(int i = 0; i < 10; i++)//assigns values
	{
		addr = (b + i);
		*addr = i;
	}
	
	for(int a = 0; a < 10; a++)//prints
	{
		printf("%d \n", b[a]);
	}

	free(b);//free's it
	return 0;
}
