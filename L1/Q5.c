#include <stdlib.h>
#include <stdio.h>

int * arr;
int size;
int num;
void add(int x)
{
    if(num == size)
    {
        int newSize = (size * 2);//change to + 1 for the first part
        arr = realloc(arr, newSize*sizeof(int));
        size = newSize;
    }
    
    arr[num] = x;//assigns a value to a pointer
    num++;
}

void removen(int i)
{
   arr[i] = NULL;
}

int get(int i)
{
    return arr[i];
}

int main()
{
    size = 10;
    num = 0;
    arr = (int*) malloc(size*sizeof(int));//creates array of size n(10)
    
    for(int i=0; i < 10000000; i++)
    {
        add(rand()%50);
    }
    
	return 0;
}

// time using +1 was 0m0.742s
// time using *2 was 0m0.097s
// The time using *2 to double the array was much faster then just adding one each time.