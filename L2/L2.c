#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int shared = 0;
pthread_mutex_t mutex_;

void* run(void *arg)//function to run the threads
{
    //no locks in this function to properly count how many shares there are
    //this is a race condition where it may not execute everything
    for(int i = 0; i < 1000; i++)
    {
        shared++;//counts the # of times the thread runs
    }
    pthread_exit(NULL);
}

int main (int argc, char *argv[])
{
    int check;//checkis if thread can be created
    int num = 1000;//number of threads to run
    pthread_t threads[num];
    for(int i = 0; i < num; i++)
    {
        //creates the threads
        if (check = pthread_create(&threads[i], NULL, run, (void *)NULL))
        {
            printf("pthread_create() error code is %d\n", check);
            exit(-1);
        }
    }
    
    for(int i = 0; i < 1000; i++)//joins threads
    {
        pthread_join(threads[i], NULL);
    }
    
    printf("Shared=%d\n",shared);
    pthread_exit(NULL);//exits threads
}

//gcc -pthread L2.c