Colleen E. Kovar
Systems Programming I
Lab 2 - Intro to Concurrent Programming
email: cek74@drexel.edu


This is a program to simulate 100 people/threads that enter a stadium and them getting up 1000 times and returning to their seats. This consists of one threaded function that invokes via 100 pthreads and increments a shared variable. What I found from this test was that when the locks were outside of the loop it ran almost 10x faster then when the locks were in the loop.

I wrote my Program in Xcode on a macOS Sierra and tested it in my terminal using gcc to text it’s functionality in an easier manner. There shouldn’t be any issues running it on a different machine with the makefile, but I’m not completely sure. You might need to add --std=c99.

There is nothing else that is needed to run this program. You can run it by typing,
make L2, make L2_in, make L2_out (this is without showing the times). I ran the program 10 times manually to get the average. I added the times and shares that I got when I ran the program 10 times. It shouldn’t crash under any circumstances and it does work no outside assumptions were made when implementing this code.

I didn’t have any difficulties with this lab and I thought this is a good into into locking/showing the most effective way to lock and unlock something to make sure it is finished. I kind of wish this was due before the networking assignment, so that it shows a simple example of threading and how to lock and unlock.