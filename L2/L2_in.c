#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int shared = 0;
pthread_mutex_t mutex_;
//this test was almost 10x slower than when it had no locks, but it's more accurate
void* run(void *arg)//function to run the threads
{
    for(int i = 0; i < 1000; i++)
    {
        //locks are move in the loop
        pthread_mutex_lock(&mutex_);
        shared++;//counts the # of times the thread runs
        pthread_mutex_unlock(&mutex_);
    }
    pthread_exit(NULL);
}

int main (int argc, char *argv[])
{
    int check;//checkis if thread can be created
    int num = 1000;//number of threads to run
    pthread_t threads[num];
    pthread_mutex_init(&mutex_, NULL);//initialize mutex
    for(int i = 0; i < num; i++)
    {
        //creates the threads
        if (check = pthread_create(&threads[i], NULL, run, (void *)NULL))
        {
            printf("pthread_create() error code is %d\n", check);
            exit(-1);
        }
    }
    
    for(int i = 0; i < 1000; i++)//joins thread
    {
        pthread_join(threads[i], NULL);
    }
    printf("Shared=%d\n",shared);
    pthread_exit(NULL);//exits threads
    
}

//gcc -pthread L2_in.c