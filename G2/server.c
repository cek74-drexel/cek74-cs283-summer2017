#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "csapp.h"
#include "miniRSA.h"

//to transfer the encryption/decryption info entered
struct key {
    long e;
    long c;
    long d;
    long dc;
};
//globals to properly connect the functions
struct key* k;
int conn;

void* c_write(void* args) //takes the message and encrypts it
{
    rio_t rio;
    char* buf = (char*) malloc(MAXLINE*sizeof(char));//buffer
    while (1)
    {
        printf("Enter message or quit by typing \\bye:"); fflush(stdout);
        Fgets(buf, MAXLINE, stdin);//flushes out the message and reads it
        if(strncmp(buf, "\\bye\n", strlen(buf)) != 0)//working protocol
        {
            for(int i = 0; buf[i+1] != NULL; i++)//for each char in buf encrypt
            {
                printf("%c", buf[i]);
                long ascii = (long) buf[i];
                buf[i] = endecrypt(ascii, k->e, k->c);
                printf("%d ", buf[i]);
            }
            printf("\nencrtyped\n");
            write(conn, buf, strlen(buf));//write it to the socket
            //pthread_exit(NULL);
            
        }
        else
        {
            //free's the buffer
            free(buf);
            printf("Logging off...\n");
            pthread_exit(NULL);//properly terminates when \bye is typed
            exit(0);
        }
    }
    
}

void* c_read(void* args)
{
    char j;
    rio_t rio;
    size_t n;
    char p[MAXLINE];//for decrypted message
    char* buf = (char*) malloc(MAXLINE*sizeof(char));//buffer
    Rio_readinitb(&rio, conn);
    while ((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0)//get socket line
    {
        for(int i = 0; buf[i] != NULL; i++)//loop the chars to decrypt them
        {
            j = endecrypt(buf[i], k->d, k->c);
            p[i] = j;
        }
        //rio_writen(conn, buf, strlen(buf));
        write(conn,p,n);
        printf("\ndecrypted\n");
        //pthread_exit(NULL);
    }
    //free's the buffer
    free(buf);
}

int main(int argc, char **argv)
{
    //initializes some variables/threads
    int listenfd, connfd, port, clientlen;
    struct sockaddr_in clientaddr;
    pthread_t threads[2];

    k = (struct key*) malloc(sizeof(struct key));//defines k to use in outside functions
    port = atoi(argv[1]);
    k->e = atoi(argv[2]);
    k->c = atoi(argv[3]);
    k->d = atoi(argv[4]);
    k->dc = atoi(argv[5]);
    
    //listens for and accepts connected ports
    listenfd = open_listenfd(port);
    clientlen = sizeof(clientaddr);
    connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
    
    //creates threads
    if(pthread_create(&threads[0], NULL, c_write, (void *)NULL)) {
        fprintf(stderr, "Threading error.");
        exit(1);
    }
    pthread_join(threads[0], NULL);
    if(pthread_create(&threads[1], NULL, c_read, (void *)NULL)) {
        fprintf(stderr, "Threading error.");
    }
    
    //exits, free's and closes everything
    free(k);
    close(conn);
    exit(0);
}

//gcc server.c csapp.c csapp.h miniRSA.c miniRSA.h -pthread