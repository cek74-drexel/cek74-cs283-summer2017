#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "miniRSA.h"

int main(int argc, char *argv[])
{
    int nth, mth;
    long a, b, c, d, e, m;
    //takes in the n and m values and converted to int/long
    int arg1 = atoi(argv[1]);
    int arg2 = atoi(argv[2]);
    nth = arg1;
    mth = arg2;
    //check to make sure they're not the same number
    if(nth == mth)
    {
        printf("Try two different numbers.\n");
        return 0;
    }
    //computed the values needed
    a = NPrime((long)nth);
    b = NPrime((long)mth);
    c = a*b;
    m = totient(a) * totient(b);
    
    e = m*2;//finds e starting at m*2
    while (1)
    {
        if(GCD(e, c) == 1 && GCD(e, m) == 1 && e%m != 1)
        {
            break;
        }
        else
            e++;
    }
    //then compute the d
    d = endecrypt(e, totient(m)-1, m);
    
    printf("%dth prime = %ld, %dth prime = %ld, c = %ld, m = %ld, e = %ld, d = %ld, Public Key = (%ld, %ld), Private Key = (%ld, %ld)\n", nth, a, mth, b, c, m, e, d, e, c, d, c);
    
    
    
    return 0;
}

//gcc genkey.c miniRSA.c miniRSA.h

