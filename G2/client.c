#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "csapp.h"
#include "miniRSA.h"

//to transfer the encryption/decryption info entered
struct key {
    long e;
    long c;
    long d;
    long dc;
};
//globals to properly connect the functions
struct key* k;
int conn;

void* c_write(void* args) //takes the message and encrypts it
{
    rio_t rio;
    char* buf = (char*) malloc(MAXLINE*sizeof(char));//buffer
    while (1)
    {
        printf("Enter message or quit by typing \\bye:"); fflush(stdout);
        Fgets(buf, MAXLINE, stdin);//flushes out the message and reads it
        if(strncmp(buf, "\\bye\n", strlen(buf)) != 0)//working protocol
        {
            for(int i = 0; buf[i+1] != NULL; i++)//for each char in buf encrypt
            {
                printf("%c", buf[i]);
                long ascii = (long) buf[i];
                buf[i] = endecrypt(ascii, k->e, k->c);
                printf("%d ", buf[i]);
            }
            printf("\nencrtyped\n");
            write(conn, buf, strlen(buf));//write it to the socket
            //pthread_exit(NULL);
            
        }
        else
        {
            //free's the buffer
            free(buf);
            printf("Logging off...\n");
            pthread_exit(NULL);//properly terminates when \bye is typed
            exit(0);
        }
    }
    
}

void* c_read(void* args)
{
    char j;
    rio_t rio;
    size_t n;
    char p[MAXLINE];//for decrypted message
    char* buf = (char*) malloc(MAXLINE*sizeof(char));//buffer
    Rio_readinitb(&rio, conn);
    while ((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0)//get socket line
    {
        for(int i = 0; buf[i] != NULL; i++)//loop the chars to decrypt them
        {
            j = endecrypt(buf[i], k->d, k->c);
            p[i] = j;
        }
        //rio_writen(conn, buf, strlen(buf));
        write(conn,p,n);
        printf("\ndecrypted\n");
        //pthread_exit(NULL);
    }
    //free's the buffer
    free(buf);
}


int main(int argc, char **argv)
{
    //initializes some variables/threads
    int conn;
    char *host;
    int port;
    pthread_t threads[2];
    
    k = (struct key*) malloc(sizeof(struct key));//defines k to use in outside functions
    host = argv[1];
    port = atoi(argv[2]);
    k->e = atoi(argv[3]);
    k->c = atoi(argv[4]);
    k->d = atoi(argv[5]);
    k->dc = atoi(argv[6]);
    
    //opens port on the host
    conn = Open_clientfd(host, port);
    if(conn < 0) {
        fprintf(stderr, "Error connecting client\n");
    }
    
    //creates threads
    if(pthread_create(&threads[0], NULL, c_write, (void *)NULL)) {
        fprintf(stderr, "Threading error.");
        exit(1);
    }
    
    if(pthread_create(&threads[1], NULL, c_read, (void *)NULL)) {
        fprintf(stderr, "Threading error.");
    }
    //joins
    pthread_join(threads[0], NULL);
    
    //exits, free's and closes everything
    free(k);
    close(conn);
    exit(0);
    
}


/*
 This code does properly encrypt and decrypt messages but wont communicate or continue the connection after the encryption
 
 
 void* c_write(void* args)
 {
 printf("Enter message or quit by typing \\bye:"); fflush(stdout);
 rio_t rio;
 char* buf = (char*) malloc(MAXLINE*sizeof(char));
 place = (long*) malloc(MAXLINE*sizeof(long));
 Fgets(buf, MAXLINE, stdin);
 if(strncmp(buf, "\\bye\n", strlen(buf)) != 0)
 {
 for(int i = 0; buf[i+1] != NULL; i++)
 {
 long ascii = (long) buf[i];
 place[i] = endecrypt(ascii, k->e, k->c);
 printf("%ld ", place[i]);
 }
 printf("\nencrtyped\n");
 }
 else
 {
 printf("Logging off...\n");
 pthread_exit(NULL);
 }
 pthread_exit(NULL);
 
 }
 
 void* c_read(void* args)
 {
 char j;
 char p[MAXLINE];
 for(int i = 0; place[i] != NULL; i++)
 {
 j = endecrypt(place[i], k->d, k->c);
 p[i] = j;
 }
 fwrite(p, 1, sizeof(place), stdout);
 printf("\ndecrypted\n");
 pthread_exit(NULL);
 }
 */

//gcc client.c csapp.c csapp.h miniRSA.c miniRSA.h -pthread