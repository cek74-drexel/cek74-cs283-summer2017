#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "miniRSA.h"

long isPrime(long n)//determines if a number is prime or not
{
    long top = n/2;
    long p = 1;
    for(int i = 2; i <= top; i++)
    {
        if(n%i == 0)//divides by everything below half of it's value
        {
            p = 0;
            break;
        }
    }
    return p;
}

long NPrime(long n)//determines the nth prime number
{
    if(n < 1)
    {
        return 0;//can't be lower than 1
    }
    if(n == 1)
    {
        return 2;//if it equals 1 it's 2
    }
    
    int ct = 0;
    int ck = 3;
    while(1)
    {
        if(isPrime(ck))//checks to see if it's prime
        {
            ct++;
        }
        if(ct == n)//the count equals the nth prime
        {
            return ck;
        }
        ck++;
    }
}

long GCD(long a, long b)//finds the greatest common denominator of the 2 numbers
{
    if(b)
    {
        return GCD(b, (a % b));//recursively calls until b is "false"
    }
    else
    {
        if(a < 0)//returns the positive a value
        {
            return -a;
        }
        else
        {
            return a;
        }
    }
}

long coprime(long x)//computes the coprime of an input
{
    long c = (x * 1.5) + 1;
    
    while(GCD(x, c) != 1)
    {
        c++;
    }
    return c;
}

long modulo(long a, long b, long c)//a^b % c for large values by breaking the exponent down to smaller bits
{
    int j = 1;
    for(int ep = 1; ep < (b + 1); ep++)
    {
        j = (j * a) % c;
    }
    return j;
}

long endecrypt(long msg_or_cipher, long key, long c)//modulo of the inputs
{
    return modulo(msg_or_cipher, key, c);
}

long mod_inverse(long base, long m)//the base to the totient(m) - 1 modulo m
{
    return modulo(base, totient(m) - 1, m);
}

long totient(long n)//calculates the totient (a-1)(b-1)
{
    double p = n;
    for(int i = 2; i <= n ; i++)
    {
        if(n % i == 0 && isPrime(i))
        {
            p *= 1 - (1.0 / i);
        }
    }
    return p;
}
