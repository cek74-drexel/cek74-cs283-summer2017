#include <stdlib.h>
#include <stdio.h>
#include "miniRSA.h"

int main(int argc, char *argv[])
{
    long e, c;
    //takes in 2 inputs changing them to int/long values
    long arg1 = atoi(argv[1]);
    long arg2 = atoi(argv[2]);
    e = arg1;
    c = arg2;
    
    //from the e and c find m and from m can find d
    int m = totient(c);
    int d = endecrypt(e, totient(m)-1, m);
    int dc = d*c;
    printf("m = %d, d = %d dc = %d\n", m, d, dc);
    
    
    return 0;
}


//gcc keycrack.c miniRSA.c miniRSA.h
//4th prime = 7, 7th prime = 17, c = 119, m = 96, e = 197, d = 77, Public Key = (197, 119), Private Key = (77, 119)