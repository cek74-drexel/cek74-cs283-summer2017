#ifndef miniRSA
#define miniRSA

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//miniRSA header with function names
long NPrime(long n);
long isPrime(long n);
long coprime(long x);
long endecrypt(long msg_or_cipher, long key, long c);
long GCD(long a, long b);
long mod_inverse(long base, long m);
long modulo(long a, long b, long c);
long totient(long n);

#endif