Colleen E. Kovar
Systems Programming I
Assignment 2 - Network Programming
email: cek74@drexel.edu


This assignment is a chat server and client communicating between each other, encrypting and decrypting the messages sent across. This includes functions of a mini RSA, generating a key from 2 inputs, cracking the key using e and c snd then using the information you have to be able to communicate between the client and the server

I wrote my Program in Xcode on a macOS Sierra and tested it in my terminal using gcc to text it’s functionality in an easier manner. Then once the functionality worked I tested it in tux and used valgrind to make sure there were no leaks and all the functions still did what they were supposed to do. There shouldn’t be any issues running it on a different machine with the makefile, but I’m not completely sure. You might need to add --std=c99.

To run my program you’re going to need csapp.c and csapp.h. It can be run in the same way as the example make lines in the assignment guide. You can enter
MPRIME=# NPRIME=# make genkey
E=# C=# make keycrack
PORT=8080 E=# C=# D=# DC=# make server
SERVER=tux64-11.cs.drexel.edu PORT=8080 E=# C=# D=# DC=# make client
Genkey, keycrack, and miniRSA all work and I’ve tested them with reasonable numbers (not super large numbers to try and completely break it) and the encryption and decryption does work for all messages/numbers I’ve tried.

The server and client doesn’t fully work in that it won’t display the message to each other, but it’s still threading between the two and writing to the socket(at least at one end). Also once you enter your message and it waits at an empty line if you enter the encrypted letters that show up it will still decrypt. The commented code is a working self-chat, as in it only communicates between itself, but it will still properly encrypt and decrypt. The problems I was having was connecting the sockets.

This assignment was very interesting. It took a fair amount of time to completely understand what somethings were or where they were supposed to be, but I was able to ask for some help/clarification and get things working, other things took more time and understanding. Getting it to properly communicate was difficult for me, but over all fair/good assignment.
