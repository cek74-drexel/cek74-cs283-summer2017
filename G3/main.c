#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include "csapp.h"

//max size of grid and number of grids because of file scope problems
#define N_MAX  10
#define M_MAX 20

//board game struct
struct board
{
    int table[N_MAX][N_MAX];
};

//globals to accurately keep track of the board
int size;
int games;
//keeps track of the
int out[M_MAX];
int in[M_MAX];
int cpids[M_MAX];
int live[M_MAX];
//keeps track of pipes/turn
int par = 1;
//the send ID
int sID = 0;
//the recieve ID
int rID = 0;
//parent pid
int ppid = 0;
//what the parent is
char parent = 'x';
//what the child is
char child = 'o';
//what an ampty space is
char isOpen = '-';
//global boards to transfer the information between functions more easily
struct board *working;
struct board *pBoard;
struct board cBoard;

//reaps children and returns if they have been killed
int deadC()
{
    pid_t pid;
    int s;
    int chil;
    while ((pid = waitpid(-1, &s, WNOHANG)) > 0)
    {
        chil = pid;
    }
    
    for(int i = 0; i < games; i++)//checks to see if one is still alive
    {
        if(live[i] == 1)
        {
            return 0;
        }
    }
    return 1;
    
}


int turn(int rfid)//reads pipe and returns the column
{
    int n;
    int cr;
    if((n=read(rfid, &cr, sizeof(int))) >= 0)
    {
        return cr;
    }
    else
    {
        //returns negitive if there is an error
        return -1;
    }
}

void place(int c, char types)//places parent or child in the highest place it is able in the column
{
    if((c >= 0) && (c < size))
    {
        for(int i = size-1; i >= 0; i--)
        {
            if(working->table[c][i] == isOpen)//if it's open place piece
            {
                working->table[c][i]=types;
                break;
            }
        }
    }
}

char win(char types)//checks for a winner and returns the type of the winner
{
    for(int i=0; i< size;i++)//checks vertically
    {
        for(int j=0; j+3< size;j++)
        {
            if( (working->table[i][j]==types) && (working->table[i][j+1]==types) && (working->table[i][j+2]==types) && (working->table[i][j+3]==types))
            {
                return types;
            }
        }
    }
    
    for(int i=0; i+3< size;i++)//checks horizontally
    {
        for(int j=0; j< size;j++)
        {
            if( (working->table[i][j]==types) && (working->table[i+1][j]==types) && (working->table[i+2][j]==types) && (working->table[i+3][j]==types))
            {
                return types;
            }
        }
    }
    
    for(int i=0; i+3< size;i++)//checks diagonally
    {
        for(int j=0; j+3< size;j++)
        {
            if(
               
               (
                (working->table[i][j]==types) && (working->table[i+1][j+1]==types) && (working->table[i+2][j+2]==types) && (working->table[i+3][j+3]==types)
                ) || ((working->table[i][j+3]==types) && (working->table[i+1][j+2]==types) && (working->table[i+2][j+1]==types) && (working->table[i+3][j]==types))
               
               )
            {
                return types;
            }
        }
    }
    
    return 0;
}

void done(char winner)//takes the char type of the winner and prints all the information needed
{
    int indx;//finds the index of the child
    for(int i = 0; i < games; i++)
    {
        if(&(pBoard[i]) == working)
        {
            indx = i;
        }
    }
    
    live[indx] = 0;
    int childPID = cpids[indx];
    kill(childPID,SIGKILL);//to properly kill it before it turns into a zombie
    
    if(winner == parent)//finds winner type
    {
        printf("The parent won\n");
    }
    else if(winner == child)
    {
        printf("The child won\n");
    }
    else
    {
        printf("It's a tie\n");
    }
    
    for(int j=0; j < size;j++)//prints the board
    {
        for(int i=0; i < size;i++)
        {
            if(working->table[i][j] == parent)
            {
                printf("\033[1;31m%c \033[0m",working->table[i][j]);
            }
            else if(working->table[i][j] == child)
            {
                printf("\033[0;36m%c \033[0m",working->table[i][j]);
            }
            else
            {
                printf("%c ",working->table[i][j]);
            }
            //free(working->table[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

int full()//checks to see if the board game is full
{
    for(int i=0; i < size;i++)
    {
        for(int j=0; j < size;j++)
        {
            if(working->table[i][j] == isOpen)
            {
                return 0;
            }
        }
    }
    return 1;
}

int cFull(int c)//checks if a column is full and no longer usable
{
    if((c >=0) && (c < size))
    {
        for(int j = 0; j < size;j++)
        {
            if(working->table[c][j] == isOpen)
            {
                return 0;
            }
        }
        return 1;
    }
}

int dgame(char types)//more unsophisticated game for the child to play
{
    int randC;
    randC = rand()%size;//chooses a random column to
    
    if(full())
    {
        if(par)
        {
            done(isOpen);//checks if board is full ends the game
            return -1;
        }
        else
        {
            return -1;
        }
    }
    
    while(cFull(randC))//find another random column to put the peice within the dimentions of the board
    {
        randC = rand()%size;
    }
    
    place(randC,types);//places it
    
    if(win(types) == types)//check if it won
    {
        if(par)
        {
            done(types);
        }
        else
        {
            return randC;
        }
    }
    return randC;
}

void deletes(int c)//removes last piece in column
{
    //goes through the table and checks is the space isn't empty then makes it empty
    if((c >=0) && (c <= size-1))
    {
        for(int j=0; j< size;j++)
        {
            if(working->table[c][j] != isOpen)
            {
                working->table[c][j] = isOpen;
                return;
            }
        }
    }
}

int sgame(char types)//tries to find the smartest move to make
{
    for(int i = 0; i < size; i++)//tries putting pece in every column
    {
        if(cFull(i)==0)
        {
            place(i,types);//first places it then checks if it's  win
            if(win(types) == types)
            {
                if(par)
                {
                    done(types);//if it is a win it runs done to end the game
                }
                else
                {
                    return i;//returns column that was played
                }
            }
            else
            {
                deletes(i);//removes it, if it's not a in
            }
        }
    }
    
    int opp;//tries to block the opponent from winning
    if(types == parent)
    {
        opp = child;
    }
    else if(types == child)
    {
        opp = parent;
    }
    
    for(int i = 0; i < size; i++)//goes through the columns looking for the opponents piece
    {
        if(cFull(i) == 0)
        {
            place(i, opp);
            if(win(opp) == opp)//if the opp can wun in that spot, block it
            {
                deletes(i);
                place(i, types);
                return i;
            }
            else
            {
                deletes(i);//otherwise delete it
            }
        }
    }
    return dgame(types);//no other smart move to make
}


void BandF(int * cwrite, int * cread, int * pwrite, int * pread)
{
    //store the pipe id's
    int ptoc[2];
    int ctop[2];
    if(pipe(ptoc) < 0 || pipe(ctop) < 0)//checks if there is a pipe error
    {
        printf("Pipe creation error.\n");
    }
    
    //assigns the values
    *cwrite = ptoc[1];
    *cread = ctop[0];
    *pwrite = ctop[1];
    *pread = ptoc[0];
}

void wrap(int *childPid, int *to, int *from)
{
    int cwrite, cread, pwrite, pread;
    BandF(&cwrite, &cread, &pwrite, &pread);//cheacks pipes and gets values
    
    int pid = fork();//forks it
    
    if(pid == 0)
    {
        //keeps track of information globally and communicates
        par = 0;
        sID = pwrite;
        rID = pread;
        ppid = getppid();
        par = 0;
        working = &cBoard;//sets playing board
        
        
        //loops until parent makes first move
        while(1)
        {
            int col = turn(rID);
            place(col, parent);
            int colp = sgame(child);
            if(write(sID, &colp, sizeof(int)) != sizeof(int))
            {
                printf("Write pipe error.\n");
            }
        }
        
        exit(-1);
    }
    else
    {
        //keeps track of information
        par = 1;
        *childPid = pid;
        *to = cwrite;
        *from = cread;
    }
}

void create(struct board *b)//creates a board
{
    for(int i = 0; i < size;i++)
    {
        for(int j = 0; j < size; j++)
        {
            b->table[i][j] = isOpen;
        }
    }
}

int main(int argc, char** argv)
{
    //sets dimentions and how many games/children there will be
    size = atoi(argv[1]);
    games = atoi(argv[2]);
    srand(time(NULL));//for random
    
    if(size < 4 || size > 10 || games > 20)//set limit of size and children
    {
        printf("Please enter a size greater than 4, but less then 10 and no games more then 20!\n");
        return 0;
    }
    
    pBoard = malloc(games*sizeof(struct board));//initializes parent board
    for(int i = 0; i < games; i++)//for the amount of games there are creat that many parent boards
    {
        create(&pBoard[i]);
        live[i] = 1;
    }
    
    create(&cBoard);//creat the childs board
    int tempC;//for function calling
    int toID;
    int fromID;
    for(int i = 0; i < games; i++)//wrapper for child games
    {
        wrap(&tempC,&toID,&fromID);
        out[i] = toID;
        in[i] = fromID;
        cpids[i] = tempC;
    }
    
    while(1)
    {
        for(int i = 0; i < games; i++)
        {
            if(live[i])//parent move for each game
            {
                working = &pBoard[i];//creates the board to play
                int play = dgame(parent);//notifies that perent should play
                //sends the message
                if(write(out[i], &play , sizeof(int)) != sizeof(int))
                {
                    printf("Pipe error.\n");
                }
            }
        }
        
        for(int i = 0; i < games; i++)//child moves for each game
        {
            if(live[i])
            {
                //waits for child move
                int cread = turn(in[i]);
                working = &pBoard[i];//parent board is the working board paying
                place(cread, child);
                //checks for win
                if(win(child) == child)
                {
                    done(child);
                }
                else if(full())
                {
                    done(isOpen);
                }
            }
        }
        
        if(deadC())//checks if dead and destroys/free's everything
        {
            free(pBoard);
            exit(0);
        }
    }
    return 0;
}