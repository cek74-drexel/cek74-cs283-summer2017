#!/usr/bin/python
import sys
from multiprocessing import Pool

def is_number(s): #finds if there is a number in the string
    try:
        float(s)
        return True
    except ValueError:
        return False

def getValByKey(row, col):
    global header # use the header row from the main script, it's just row 1 of the CSV
    result = ''
    idx = 0
    
    for keycol in header.split(','):
        if keycol == col:
            result = row.split(',')[idx]
            break
        idx = idx + 1
    
    result = result.strip().replace('\'', '')
    return result


def mapper(process_data):#takes in the data set and finds which airport has which delays
    result = dict()
    for row in process_data:
        key = getValByKey(row, 'Origin') #the key is by it's origin
        if not (key in result):
            result[key] = []
        #computes the security and waether delays
        sec = getValByKey(row, 'SecurityDelay')
        weth = getValByKey(row, 'WeatherDelay')
        if (sec == '0.0' and weth == '0.0'):
            val = 0
        elif (sec == '0.0' and weth != '0.0'):
            val = weth
        elif (sec != '0.0' and weth == '0.0'):
            val = sec
        else:
            val = weth + sec

        result[key].append(val)
        return result

def partition(mappings_list):
    # mappings is an array of dicts of origin keys to the values,
    # which we want to aggregate into a single dict organized by key,
    # so that each reducer will receive only values from the same key
    
    result = dict()
    for mappings in mappings_list:
        for key in mappings:
            for val in mappings[key]:
                if not (key in result):
                    result[key] = []
                
                result[key].append(val)
    
    return result

def reducer(tuple_list):
    # We have a particular key, which is the origin;
    # iterate over it's values and compute the average when needed
    # returning a dict for the origin and a dict of average values for each airport
    
    data = dict()
    result = dict()
    vals = dict()
    
    for tuple in tuple_list:
        var = 0
        if tuple[0] == '' or is_number(tuple[0]) == True:
            i = 1
            #does the calculations here
            while i < len(tuple):
                if tuple[i] != '':
                    temp = tuple[i]
                    var = var + float(temp)
                i = i + 1
            # if you want to average the values add the line
            #var = var/i
            if not (var in vals):
                #made it it's own dictionary to match up
                vals[var] = dict()
        else:
            #gets the airports
            orig = tuple[:3]
            if not (orig in data):
                data[orig] = dict()
        
        #assigns the values to the airports
        if not (var in data[orig]):
            data[orig] = [var]
        
    for orig in data:
        if not (orig in result):
            result[orig] = dict()
        
            for var in data[orig]:
                if len(data[orig]) > 0:
                    result[orig] = data[orig]
                else:
                    result[orig] = 0



    return result


filename = sys.argv[1]
processes = int(sys.argv[2])
header = ''

csvlines = []
csvfile = open(filename, 'r')
lineno = 0

for line in csvfile:
    if lineno > 0: # don't read the header line into the list to be processed
        csvlines.append(line)
    else:
        header = line
    lineno = lineno + 1

numlines = len(csvlines)
lines_per_process = numlines / processes

process_data_array = []
for i in range(processes):
    start = i * (numlines / processes)
    end = (i+1) * (numlines / processes)
    process_data_array.append(csvlines[start:end])

pool = Pool(processes=processes,)
#where the mapping shuffle and reducing happens
mapping = pool.map(mapper, process_data_array)
shuffled = partition(mapping)
reduced = pool.map(reducer, shuffled.items())

#collects the info from each airport and prints the averages or computations
for i in range(len(reduced)):
    for origin in reduced[i]:
            print "In", origin, "airport the total security + weather delay delay was", reduced[i][origin]



