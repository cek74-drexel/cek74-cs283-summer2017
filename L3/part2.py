import random
num_samples = 10000000
def inside(p):
    x, y = random.random(), random.random()
    return x*x + y*y < 1
#counting the number within the circle
count = sc.parallelize(range(0, num_samples), 2).filter(inside).count()

#calculates pi
pi = 4.0 * count / num_samples
print(pi)

sc.stop()